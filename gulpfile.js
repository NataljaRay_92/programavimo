/*
 *
 * Определяем переменные
 *
 */
var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    scss        = require('gulp-sass'),
    pug         = require('gulp-pug'),
    concat      = require('gulp-concat');

/*
 *
 * Создаем задачи (таски)
 *
 */

// Задача "scss". Запускается командой "gulp scss"
gulp.task('scss', function () {
    return gulp.src(['app/scss/all.scss'])
        .pipe(scss())
        .pipe(concat('all.css'))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
});

// Задача "js". Запускается командой "gulp js"
gulp.task('js', function () {
    return gulp.src(['app/js/**'])
        // .pipe(gulp.dest('app/js'))
        .pipe(browserSync.reload({stream: true}))
});

// Задача "pug". Запускается командой "gulp pug"
gulp.task('pug',function() {
  gulp.src(['app/pug/**/*.pug','!app/pug/blocks/**/*.pug'])
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('app'))
    .pipe(browserSync.reload({stream: true}))
});


gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('build',[
    'scss',
    'pug'
]);

// Задача "watch". Запускается командой "gulp watch"
// Она следит за изменениями файлов и автоматически запускает другие задачи

gulp.task('watch', ['browser-sync', 'scss', 'js', 'pug'],function () {
    gulp.watch('app/scss/**/*.scss', ['scss']);

    gulp.watch('app/js/**/*.js', ['js']);
    gulp.watch('app/pug/**/*.pug', ['pug']);
    // gulp.watch(['app/pug/**/*.pug','app/pug/blocks/**/*.pug'], ['pug']);

    gulp.watch('app/*.html', browserSync.reload);

});

// Default Task
gulp.task('default', ['build', 'scss', 'pug', 'watch']);
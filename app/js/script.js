$( document ).ready(function() {
  //проверка подключения
  // console.log( "hallo!" );


  $('.map-button').on('click', function () {
    $(this).parent().toggleClass('is-open');
  })



  $("a[href*='#']").on("click", function(e){
    var anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 777);
    e.preventDefault();
    return false;
  });


  // parallax

  (function(){
    var body = document.getElementById('parallax-twin'),
      startX = -100,
      startY = -100,
      w = document.documentElement.offsetWidth,
      h = document.documentElement.offsetHeight;

    body.addEventListener('mousemove', function(evt){
      var posX = Math.round(evt.clientX / w * startX)
      var posY = Math.round(evt.clientY / h * startY)
      body.style.backgroundPosition = posX + 'px ' + posY + 'px'
    })
  })()

  // (function(){
  //   var body = document.getElementById('parallax-twin'),
  //     startX2 = -100,
  //     startY2 = -100,
  //     w2 = document.documentElement.offsetWidth,
  //     h2 = document.documentElement.offsetHeight;
  //
  //   body.addEventListener('mousemove', function(evt){
  //     var posX2 = Math.round(evt.clientX / w2 * startX2)
  //     var posY2 = Math.round(evt.clientY / h2 * startY2)
  //     body.style.backgroundPosition = posX2 + 'px ' + posY2 + 'px'
  //   })
  // })()

});